
# Pitlane monitoring
The aim of the pitlane monitoring project is to monitor and analyse telemetry data from a number of sensors installed in various models of racing cars.
The project is divided into two parts:
1. A backend that simulates the streaming of telemetry data from the track.
    The streaming must be sorted by ASC timestamp and provide the measurements of each vehicle's tyre.
    vehicle. The backend must also support basic filtering by vehicle model (selecting an individual vehicle or a subset) and include a filter by vehicle model.
    vehicle or a subset) and include a data cleaning process to correct inaccurate (negative) pressure and speed values.
    inaccurate (negative) pressure and speed values.
2. A frontend that consumes the data stream received from the backend and displays for each vehicle in real time the
for each vehicle in real time the different measurements at different levels of aggregation, as seen
meaningful to convey measurement trends.








## Build with

Frontend

![Angular](https://img.shields.io/badge/angular-%23DD0031.svg?style=for-the-badge&logo=angular&logoColor=white) 

BackEnd

![Apache Maven](https://img.shields.io/badge/Apache%20Maven-C71A36?style=for-the-badge&logo=Apache%20Maven&logoColor=white)

![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=java&logoColor=white)

DB - NoSql

![MongoDB](https://img.shields.io/badge/MongoDB-%234ea94b.svg?style=for-the-badge&logo=mongodb&logoColor=white)






## Getting Started

This is an example of how you may give instructions on setting up your project locally. To get a local copy up and running follow these simple example steps.

Prerequisites
This is an example of how to list things you need to use the software and how to install them.

Install Nodejs base on your Os

[Node](https://nodejs.org/en)

Then Open your shell cmd.exe(windows) - terminal (MacOs/Unix like)

Install npm
```bash
    npm install npm@latest -g
```

Install angular cli

```bash
    npm install -g @angular/cli
```
Install maven base on your Os - please ref

[Maven](https://maven.apache.org/download.cgi)

Install java 17 base on your Os - please ref

[Java](https://docs.aws.amazon.com/corretto/latest/corretto-17-ug/downloads-list.html)

Install docker & docker-compose base on your Os - please ref

[Docker](https://www.docker.com/)


Clone the project

```bash
  git clone https://gitlab.com/djdavi96c/pitlanemonitoring-project.git
```

Go to the project directory

```bash
  cd pitlaneMonitoring
```

Install dependencies of service

```bash
  mvn clean install
```

Wait until terminate the command or open another shell to the pitlaneMonitoringFront folder
Run this command for install the dependencies

```bash
  npm install
```




## Configuration MongoDb & Service

In the pitlaneMonitoring open shell and run the following command

edit docker-compose.yml and edit with the absolute path of the directory of the project
```bash
 /mnt/c/progetti/pitlanemonitoring-project/dump:/dump
```
with

```bash
your-path-before-folder-of-the-project/pitlanemonitoring-project/dump:/dump
```

save and exit, than in the shell launch this command

```bash
  docker-compose up -d
```

After terminate connect to mongo container with the following command

```bash
  docker exec -it pitlanemonitoring-project-db-1 sh
```
Then execute this command and wait until terminate

```bash
  mongorestore
```

When terminate write this command
```bash
  exit
```

Now you can restart docker-compose

```bash
  docker-compose restart 
```

Now service and db are up

Service can be call by the following url

```bash
  http://localhost:8082
```

For example of api avaiable follow the section API Reference



## API Reference 

#### Get all measurament with all vehicleModels

```http
  GET /api/v1/measurament
```


#### Get all type of vehicle

```http
  GET /api/v1/measurament/vehiclesType
```


#### Get all measurament for specific vehicle

```http
  GET /api/v1/measurament/vehicle/{vehicleModel}
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `vehicleModel` | `string` | **Required**. Example: Alfa Romeo Giulia GTA |

#### Get all measurament for specific set of vehicles

```http
  GET /api/v1/measurament/vehicles/{vehicleModels}
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `vehicleModels` | `string` | **Required**. Example: Alfa Romeo Giulia GTA,Porsche 911 GT3 |








## Deploy Frontend

After following the section Getting Started and Configuration MongoDb & Service now we can start Frontend.

Go to the folder pitlaneMonitoringFront inside pitlaneMonitoring and open shell
Run the following command


```bash
  ng serve
```

Now fronted is up on the following url

```bash
http://localhost:4200/
```

