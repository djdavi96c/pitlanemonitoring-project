import { TestBed } from '@angular/core/testing';

import { MeasuramentServiceService } from './measurament-service.service';

describe('MeasuramentServiceService', () => {
  let service: MeasuramentServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MeasuramentServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
