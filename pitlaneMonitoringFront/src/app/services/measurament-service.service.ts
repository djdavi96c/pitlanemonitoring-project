import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Measurament } from '../models/measurement';
import { Observable  } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MeasuramentServiceService {

  measuraments: Measurament[] = [];

  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient, public router: Router) {}

  getVehicleTypes() : Observable<string[]> {
    let api = '/api/v1/measurament/vehiclesType';
    return this.http.get<string[]>(api,{headers: this.headers});
  }

  observeMeasuraments(url:string): Observable<Measurament>{
    return new Observable<any>(obs=>{
      const es = new EventSource(url);
      es.addEventListener('message',(evt:any)=>{
        obs.next(evt.data !=null ? JSON.parse(evt.data) as Measurament: evt.data as Measurament);
      });
      return () => es.close();
    })
  }

  
  observeVehicleTypes(url:string): Observable<string>{
    return new Observable<any>(obs=>{
      const es = new EventSource(url);
      es.addEventListener('message',(evt:any)=>{
        obs.next(evt.data !=null ? JSON.parse(evt.data) as string: evt.data as string);
      });
      return () => es.close();
    })
  }


  
}
