export interface Measurament{
    id?:string;
    timestamp?:Date;
    pressure?:number;
    tyrePosition?:string;
    temperature?:number;
    angularVelocity?:number;
    linearSpeed?:number;
    vehicleModel?:string;


    
}