import { Component } from '@angular/core';
import { MeasuramentServiceService } from 'src/app/services/measurament-service.service';
import { Router } from '@angular/router';
import { Measurament } from 'src/app/models/measurement';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable, Subscription, takeWhile } from 'rxjs';
import { LazyLoadEvent } from 'primeng/api';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  public measuramentMap: Map<string, Measurament[]> = new Map();
  public loading: boolean = true;
  public api = '/api/v1/measurament';
  public vehicleTypes$: Observable<string[]>;
  public vehicleTypesList: string[] = [];
  private alive: boolean = true;

  constructor(public router: Router, public measuramentService: MeasuramentServiceService) {
    this.vehicleTypes$ = this.measuramentService.getVehicleTypes();
    this.vehicleTypes$.subscribe({
      next: (response) => {
        response.forEach(item => {
          this.measuramentMap.set(item, []);
          this.vehicleTypesList.push(item);
        })
      }
    });
  }

  ngOnInit() {
  }

  loadMeasuraments(event: LazyLoadEvent, vehicleModel: string) {
    this.loading = true;
    setTimeout(() => {
      this.measuramentService.observeMeasuraments(this.api + '/vehicle/' + vehicleModel)
        .pipe(takeWhile(() => this.alive))
        .subscribe({
          next: (response) => {
            this.measuramentMap.get(vehicleModel)?.push(response);
          }

        });
      this.loading = false;
    }, 5000);

    if (event.sortField && event.sortField!==undefined) {
      this.measuramentMap.get(vehicleModel)!.sort((data1, data2) => {
        let value1;
        let value2;
        if(data1.timestamp===data2.timestamp){
          if(typeof event.sortField != 'undefined'){
            if(event.sortField =='pressure'){
              value1= data1.pressure;
              value2= data2.pressure;
            }else if(event.sortField == 'tyrePosition'){
              value1= data1.tyrePosition;
              value2= data2.tyrePosition;
            }else if(event.sortField == 'temperature'){
              value1= data1.temperature;
              value2= data2.temperature;
            }else if(event.sortField == 'angularVelocity'){
              value1= data1.angularVelocity;
              value2= data2.angularVelocity;
            }else if(event.sortField == 'linearSpeed'){
              value1= data1.linearSpeed;
              value2= data2.linearSpeed;
            }
          }
        }
      
        let result = 0;
        
        if (value1 == null && value2 != null)
          result = -1;
        else if (value1 != null && value2 == null)
          result = 1;
        else if (value1 == null && value2 == null)
          result = 0;
        else if (typeof value1 === 'string' && typeof value2 === 'string')
          result = value1.localeCompare(value2);
        else if(value1!==undefined && value2!==undefined)
          result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

        return (event.sortOrder! * result);
      });
    }
  }

    ngOnDestroy() {
      this.alive=false;
    }
  }



