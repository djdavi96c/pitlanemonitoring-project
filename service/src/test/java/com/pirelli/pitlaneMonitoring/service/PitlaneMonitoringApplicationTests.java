package com.pirelli.pitlaneMonitoring.service;

import com.pirelli.pitlaneMonitoring.service.controller.MeasuramentController;
import com.pirelli.pitlaneMonitoring.service.service.MeasuramentService;
import com.pirelli.pitlaneMonitoring.service.service.structure.MeasuramentServiceBean;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import  static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebFluxTest(MeasuramentController.class)
class PitlaneMonitoringApplicationTests {

	@Autowired
	private WebTestClient webTestClient;

	@MockBean
	private MeasuramentService service;


	@Test
	void retrieveVehicleModels() {
		Flux<Object> vehicleModels = Flux.just("Alfa Romeo Giulia GTA","Porche 911 GT3","Audi rs");
		when(service.findAllVehicleType()).thenReturn(vehicleModels);
		Flux<Object> responseBody = webTestClient.get().uri("/api/v1/measurament/vehiclesType").exchange().expectStatus().isOk().returnResult(Object.class).getResponseBody();

		StepVerifier.create(responseBody)
				.expectSubscription()
				.expectNext("Alfa Romeo Giulia GTA")
				.expectNext("Porche 911 GT3")
				.expectNext("Audi rs")
				.verifyComplete();
	}

	@Test
	void retrieveAll() {
		MeasuramentServiceBean prod1= new MeasuramentServiceBean("sdad56145646adad",new Date(), BigDecimal.valueOf(2.54999995232),"FRONT_LEFT",BigDecimal.valueOf(9),BigDecimal.valueOf(45.4199999641),
				BigDecimal.valueOf(51.0111111535),"Alfa Romeo Giulia GTA");
		MeasuramentServiceBean prod2= new MeasuramentServiceBean("sdad561449498adad",new Date(), BigDecimal.valueOf(2.54999995232),"REAR_RIGHT",BigDecimal.valueOf(9),BigDecimal.valueOf(45.4199999641),
				BigDecimal.valueOf(51.0111111535),"Porche 911 GT3");
		MeasuramentServiceBean prod3 = new MeasuramentServiceBean("sdad561894d8adad",new Date(), BigDecimal.valueOf(2.54999995232),"REAR_RIGHT",BigDecimal.valueOf(9),BigDecimal.valueOf(45.4199999641),
				BigDecimal.valueOf(51.0111111535),"Audi rs");
		Flux<MeasuramentServiceBean> vehicleModels = Flux.just(
			prod1,prod2,prod3);
		when(service.findAll()).thenReturn(vehicleModels);
		Flux<MeasuramentServiceBean> responseBody = webTestClient.get().uri("/api/v1/measurament").exchange()
				.expectStatus()
				.isOk()
				.returnResult(MeasuramentServiceBean.class)
				.getResponseBody();

		StepVerifier.create(responseBody)
				.expectSubscription()
				.expectNext(prod1)
				.expectNext(prod2)
				.expectNext(prod3)
				.verifyComplete();
	}

	@Test
	void retrieveByVehicleModel() {
		String vehicleModel = "Alfa Romeo Giulia GTA";
		MeasuramentServiceBean prod1= new MeasuramentServiceBean("sdad56145646adad",new Date(), BigDecimal.valueOf(2.54999995232),"FRONT_LEFT",BigDecimal.valueOf(9),BigDecimal.valueOf(45.4199999641),
				BigDecimal.valueOf(51.0111111535),"Alfa Romeo Giulia GTA");
		MeasuramentServiceBean prod2= new MeasuramentServiceBean("sdad561449498adad",new Date(), BigDecimal.valueOf(2.54999995232),"REAR_RIGHT",BigDecimal.valueOf(9),BigDecimal.valueOf(45.4199999641),
				BigDecimal.valueOf(51.0111111535),"Alfa Romeo Giulia GTA");

		Flux<MeasuramentServiceBean> vehicleModels = Flux.just(
				prod1,prod2);
		when(service.findByVehicleModel(vehicleModel)).thenReturn(vehicleModels);
		Flux<MeasuramentServiceBean> responseBody = webTestClient.get().uri("/api/v1/measurament/vehicle/".concat(vehicleModel)).exchange()
				.expectStatus()
				.isOk()
				.returnResult(MeasuramentServiceBean.class)
				.getResponseBody();

		StepVerifier.create(responseBody)
				.expectSubscription()
				.expectNext(prod1)
				.expectNext(prod2)
				.verifyComplete();
	}

	@Test
	void retrieveByVehicleModels() {
		List<String> vehicleModelsToSearch = Arrays.asList("Alfa Romeo Giulia GTA","Porche 911 GT3");
		MeasuramentServiceBean prod1= new MeasuramentServiceBean("sdad56145646adad",new Date(), BigDecimal.valueOf(2.54999995232),"FRONT_LEFT",BigDecimal.valueOf(9),BigDecimal.valueOf(45.4199999641),
				BigDecimal.valueOf(51.0111111535),"Alfa Romeo Giulia GTA");
		MeasuramentServiceBean prod2= new MeasuramentServiceBean("sdad561449498adad",new Date(), BigDecimal.valueOf(2.54999995232),"REAR_RIGHT",BigDecimal.valueOf(9),BigDecimal.valueOf(45.4199999641),
				BigDecimal.valueOf(51.0111111535),"Porche 911 GT3");
		MeasuramentServiceBean prod3 = new MeasuramentServiceBean("sdad561894d8adad",new Date(), BigDecimal.valueOf(2.54999995232),"REAR_RIGHT",BigDecimal.valueOf(9),BigDecimal.valueOf(45.4199999641),
				BigDecimal.valueOf(51.0111111535),"Audi rs");
		Flux<MeasuramentServiceBean> vehicleModels = Flux.just(
				prod1,prod2);
		when(service.findByVehicleModels(vehicleModelsToSearch)).thenReturn(vehicleModels);
		StringBuilder stringBuilder = new StringBuilder("/api/v1/measurament/vehicles/");
		stringBuilder.append(vehicleModelsToSearch.toString());
		Flux<MeasuramentServiceBean> responseBody = webTestClient.get().uri(stringBuilder.toString().replace("[","").replace("]","")).exchange()
				.expectStatus()
				.isOk()
				.returnResult(MeasuramentServiceBean.class)
				.getResponseBody();

		StepVerifier.create(responseBody)
				.expectSubscription()
				.expectNext(prod1)
				.expectNext(prod2)
				.verifyComplete();
	}

}
