package com.pirelli.pitlaneMonitoring.service.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.math.BigDecimal;
import java.util.Date;

@Document(collection = "measurament")
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Data
public class Measurament {
    @Id
    private String id;

    private Date timestamp;
    @Field(targetType= FieldType.DOUBLE)
    private BigDecimal pressure;

    private String tyrePosition;

    private BigDecimal temperature;

    private BigDecimal angularVelocity;

    @Field(targetType= FieldType.DOUBLE)
    private BigDecimal linearSpeed;

    private String vehicleModel;

}
