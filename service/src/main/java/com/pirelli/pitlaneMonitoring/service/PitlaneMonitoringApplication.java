package com.pirelli.pitlaneMonitoring.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PitlaneMonitoringApplication {

	public static void main(String[] args) {
		SpringApplication.run(PitlaneMonitoringApplication.class, args);
	}

}
