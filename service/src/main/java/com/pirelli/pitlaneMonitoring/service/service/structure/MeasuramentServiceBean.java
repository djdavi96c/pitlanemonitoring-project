package com.pirelli.pitlaneMonitoring.service.service.structure;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Data
public class MeasuramentServiceBean {
    private String id;
    private Date timestamp;
    private BigDecimal pressure;
    private String tyrePosition;
    private BigDecimal temperature;
    private BigDecimal angularVelocity;
    private BigDecimal linearSpeed;
    private String vehicleModel;

}