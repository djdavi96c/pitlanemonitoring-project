package com.pirelli.pitlaneMonitoring.service.mapper;

import com.pirelli.pitlaneMonitoring.service.model.Measurament;
import com.pirelli.pitlaneMonitoring.service.service.structure.MeasuramentServiceBean;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface MeasuramentMapper {

   public MeasuramentServiceBean toService(Measurament model);

   public Measurament toModel(MeasuramentServiceBean service);

   public List<MeasuramentServiceBean> toServiceList(List<Measurament> modelList);

   public List<Measurament> toModelList(List<MeasuramentServiceBean> serviceList);

}
