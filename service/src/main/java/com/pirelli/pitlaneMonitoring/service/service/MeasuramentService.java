package com.pirelli.pitlaneMonitoring.service.service;

import com.pirelli.pitlaneMonitoring.service.service.structure.MeasuramentServiceBean;
import reactor.core.publisher.Flux;

import java.util.List;

public interface MeasuramentService {
    Flux<MeasuramentServiceBean> findAll();

    Flux<MeasuramentServiceBean> findByVehicleModel(String vehicleModel);

    Flux<MeasuramentServiceBean> findByVehicleModels(List<String> vehicleModels);

    Flux<Object> findAllVehicleType();
}