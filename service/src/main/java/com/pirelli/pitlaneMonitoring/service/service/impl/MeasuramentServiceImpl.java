package com.pirelli.pitlaneMonitoring.service.service.impl;

import com.pirelli.pitlaneMonitoring.service.mapper.MeasuramentMapper;
import com.pirelli.pitlaneMonitoring.service.repository.MeasuramentRepository;
import com.pirelli.pitlaneMonitoring.service.service.MeasuramentService;
import com.pirelli.pitlaneMonitoring.service.service.structure.MeasuramentServiceBean;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.math.BigDecimal;
import java.util.List;

@Service
public class MeasuramentServiceImpl implements MeasuramentService {
    @Autowired
    private MeasuramentRepository repository;
    private MeasuramentMapper mapper = (MeasuramentMapper) Mappers.getMapper(MeasuramentMapper.class);

    public MeasuramentServiceImpl() {
    }

    public Flux<MeasuramentServiceBean> findAll() {
        return repository.findAllByPressureGreaterThanEqualAndLinearSpeedGreaterThanEqualOrderByTimestampAsc(BigDecimal.ZERO, BigDecimal.ZERO)
                        .map(mapper::toService);


    }

    public Flux<MeasuramentServiceBean> findByVehicleModel(String vehicleModel) {
        return repository.findByVehicleModelIgnoreCaseAndPressureGreaterThanEqualAndLinearSpeedGreaterThanEqualOrderByTimestampAsc(vehicleModel, BigDecimal.ZERO, BigDecimal.ZERO)
                .map(mapper::toService);
    }

    public Flux<MeasuramentServiceBean> findByVehicleModels(List<String> vehicleModels) {
       return repository.findByVehicleModelInIgnoreCaseAndPressureGreaterThanEqualAndLinearSpeedGreaterThanEqualOrderByTimestampAsc(vehicleModels, BigDecimal.ZERO, BigDecimal.ZERO)
               .map(mapper::toService);

    }

    public Flux<Object> findAllVehicleType() {
        return repository.findDistinctByVehicleModel();
    }

}
