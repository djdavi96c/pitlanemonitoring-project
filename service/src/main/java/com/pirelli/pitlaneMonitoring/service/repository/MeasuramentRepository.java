package com.pirelli.pitlaneMonitoring.service.repository;

import com.pirelli.pitlaneMonitoring.service.model.Measurament;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface MeasuramentRepository extends ReactiveMongoRepository<Measurament,String> {

    public Flux<Measurament> findAllByPressureGreaterThanEqualAndLinearSpeedGreaterThanEqualOrderByTimestampAsc(BigDecimal pressure,BigDecimal linearSpeed);
    public Flux<Measurament> findByVehicleModelIgnoreCaseAndPressureGreaterThanEqualAndLinearSpeedGreaterThanEqualOrderByTimestampAsc(String vehicleModel,BigDecimal pressure,BigDecimal linearSpeed);

    public Flux<Measurament> findByVehicleModelInIgnoreCaseAndPressureGreaterThanEqualAndLinearSpeedGreaterThanEqualOrderByTimestampAsc(List<String> vehicleModel, BigDecimal pressure, BigDecimal linearSpeed);

    @Aggregation(pipeline = { "{ '$group': { '_id' : '$vehicleModel' } }" })
    public Flux<Object> findDistinctByVehicleModel();

}
