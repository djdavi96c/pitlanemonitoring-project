package com.pirelli.pitlaneMonitoring.service.controller;

import com.pirelli.pitlaneMonitoring.service.service.MeasuramentService;
import com.pirelli.pitlaneMonitoring.service.service.structure.MeasuramentServiceBean;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.List;

@RestController
@RequestMapping("api/v1/measurament")
@Validated
public class MeasuramentController {

    private static final Logger logger = LoggerFactory.getLogger(MeasuramentController.class);
    private final MeasuramentService service;

    @Autowired
    MeasuramentController(MeasuramentService measuramentService){
        this.service = measuramentService;
    }

    @GetMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<MeasuramentServiceBean> getAllMeasurament(){
       return service.findAll().delayElements(Duration.ofMillis(1000))
               .doOnError(error->logger.error("Error during fetch information",error))
               .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)));
    }

    @GetMapping(value="/vehiclesType")
    public Flux<Object> getAllVehicleType(){
        return service.findAllVehicleType();
    }

    @GetMapping(value = "/vehicle/{vehicleModel}",produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<MeasuramentServiceBean> getByVehicleModel(@Valid @PathVariable @NotNull @NotBlank(message = "{api.error.notVehicle.specificated}") String vehicleModel){
        return service.findByVehicleModel(vehicleModel).delayElements(Duration.ofMillis(1000))
                .doOnError(error->logger.error("Error during fetch information for vehicle {}",vehicleModel,error))
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)));
    }

    @GetMapping(value = "/vehicles/{vehicleModels}",produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<MeasuramentServiceBean> getByVehicleModels(@Valid @PathVariable @NotNull List<String> vehicleModels){
        return service.findByVehicleModels(vehicleModels).delayElements(Duration.ofMillis(1000))
                .doOnError(error->logger.error("Error during fetch information for vehicles {}",vehicleModels,error))
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)));
    }
}
